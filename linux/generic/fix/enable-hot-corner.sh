#!/bin/bash

echo "Enabling GNOME hot corner"
gsettings set org.gnome.desktop.interface enable-hot-corners true
# source:: https://stackoverflow.com/questions/67169789/how-can-i-re-disable-the-hot-corner-in-gnome-shell-40
