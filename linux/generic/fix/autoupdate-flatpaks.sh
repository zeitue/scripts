#!/bin/bash

# Add support to systemd to auto update flatpaks
cat <<EOF | sudo dd status=none of="/etc/systemd/user/update-user-flatpaks.service"
[Unit]
Description=Update user Flatpaks

[Service]
Type=oneshot
ExecStart=/usr/bin/flatpak --user update -y

[Install]
WantedBy=default.target
EOF


cat <<EOF | sudo dd status=none of="/etc/systemd/system/update-system-flatpaks.service"
[Unit]
Description=Update system Flatpaks
After=network-online.target
Wants=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/flatpak --system update -y

[Install]
WantedBy=multi-user.target
EOF


cat <<EOF | sudo dd status=none of="/etc/systemd/user/update-user-flatpaks.timer"
[Unit]
Description=Update user Flatpaks daily

[Timer]
OnCalendar=daily
Persistent=true

[Install]
WantedBy=timers.target
EOF


cat <<EOF | sudo dd status=none of="/etc/systemd/system/update-system-flatpaks.timer"
[Unit]
Description=Update system Flatpaks daily

[Timer]
OnCalendar=daily
Persistent=true

[Install]
WantedBy=timers.target
EOF

sudo systemctl --system enable --now update-system-flatpaks.timer
sudo systemctl enable --global update-user-flatpaks.timer
systemctl --user enable --now update-user-flatpaks.timer
