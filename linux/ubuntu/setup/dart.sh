#!/bin/bash

echo "Installing Dart"
sudo apt-get update
sudo apt-get install -y apt-transport-https
wget -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub |\
sudo gpg --dearmor -o /usr/share/keyrings/dart.gpg
echo 'deb [signed-by=/usr/share/keyrings/dart.gpg arch=amd64] https://storage.googleapis.com/download.dartlang.org/linux/debian stable main' |\
sudo dd status=none of=/etc/apt/sources.list.d/dart_stable.list

sudo apt-get update
sudo apt-get install -y dart

# Add into path
cat <<EOF | sudo dd status=none of="/etc/profile.d/dart.sh"
export PATH="\$PATH:/usr/lib/dart/bin"
EOF

